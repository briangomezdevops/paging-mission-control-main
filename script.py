#!/usr/bin/env python3
import sys
from datetime import datetime, timedelta
import json

def analyze_data(data):
    alerts = []
    for sat_id, readings in data.items():
        readings.sort(key=lambda x: x['timestamp'])
        
        tstat_alert_times = []
        batt_alert_times = []

        for reading in readings:
            if reading['component'] == "TSTAT" and reading['raw_value'] > reading['red_high']:
                tstat_alert_times.append(reading['timestamp'])

            if reading['component'] == "BATT" and reading['raw_value'] < reading['red_low']:
                batt_alert_times.append(reading['timestamp'])

        for i in range(len(tstat_alert_times) - 2):
            if tstat_alert_times[i+2] - tstat_alert_times[i] <= timedelta(minutes=5):
                alerts.append({
                    "satelliteId": sat_id,
                    "severity": "RED HIGH",
                    "component": "TSTAT",
                    "timestamp": tstat_alert_times[i+2].isoformat() + "Z"
                })

        for i in range(len(batt_alert_times) - 2):
            if batt_alert_times[i+2] - batt_alert_times[i] <= timedelta(minutes=5):
                alerts.append({
                    "satelliteId": sat_id,
                    "severity": "RED LOW",
                    "component": "BATT",
                    "timestamp": batt_alert_times[i+2].isoformat() + "Z"
                })

    return alerts

def main():
    data = {}

    with open(sys.argv[1], 'r') as my_file:
        for line in my_file.readlines():
            parts = line.split("|")
            timestamp = datetime.strptime(parts[0], '%Y%m%d %H:%M:%S.%f')
            sat_id = int(parts[1])
            red_high = float(parts[2])
            red_low = float(parts[5])
            raw_value = float(parts[6])
            component = parts[7].strip()

            if sat_id not in data:
                data[sat_id] = []

            data[sat_id].append({
                "timestamp": timestamp,
                "red_high": red_high,
                "red_low": red_low,
                "raw_value": raw_value,
                "component": component
            })

    alerts = analyze_data(data)
    print(json.dumps(alerts, indent=4))

if __name__ == "__main__":
    main()
