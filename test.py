import unittest
from datetime import datetime, timedelta
from script import analyze_data

class TestSatelliteMonitor(unittest.TestCase):

    def test_analyze_data(self):
        data = {
            1000: [
                {"timestamp": datetime(2018, 1, 1, 23, 1, 35, 1), "red_high": 70, "red_low": 30, "raw_value": 75, "component": "TSTAT"},
                {"timestamp": datetime(2018, 1, 1, 23, 1, 36, 1), "red_high": 70, "red_low": 30, "raw_value": 72, "component": "TSTAT"},
                {"timestamp": datetime(2018, 1, 1, 23, 1, 37, 1), "red_high": 70, "red_low": 30, "raw_value": 76, "component": "TSTAT"},
                {"timestamp": datetime(2018, 1, 1, 23, 1, 5, 521), "red_high": 70, "red_low": 30, "raw_value": 25, "component": "BATT"},
                {"timestamp": datetime(2018, 1, 1, 23, 1, 6, 521), "red_high": 70, "red_low": 30, "raw_value": 26, "component": "BATT"},
                {"timestamp": datetime(2018, 1, 1, 23, 1, 7, 521), "red_high": 70, "red_low": 30, "raw_value": 29, "component": "BATT"}
            ]
        }

        expected_output = [
            {
                "satelliteId": 1000,
                "severity": "RED HIGH",
                "component": "TSTAT",
                "timestamp": "2018-01-01T23:01:37.000001Z"
            },
            {
                "satelliteId": 1000,
                "severity": "RED LOW",
                "component": "BATT",
                "timestamp": "2018-01-01T23:01:07.000521Z"
            }
        ]

        result = analyze_data(data)
        self.assertEqual(result, expected_output)

if __name__ == "__main__":
    unittest.main()
